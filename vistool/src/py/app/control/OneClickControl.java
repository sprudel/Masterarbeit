package py.app.control;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;


import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.embed.swing.JFXPanel;
//import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import prefuse.Constants;
import prefuse.Display;
import prefuse.Visualization;
import prefuse.action.ActionList;
import prefuse.action.RepaintAction;
import prefuse.action.assignment.ColorAction;
import prefuse.action.assignment.FontAction;
import prefuse.action.layout.RandomLayout;
import prefuse.controls.ControlAdapter;
import prefuse.controls.DragControl;
import prefuse.controls.PanControl;
import prefuse.controls.ZoomControl;
import prefuse.controls.ZoomToFitControl;
import prefuse.data.Graph;
import prefuse.data.Node;
import prefuse.render.DefaultRendererFactory;
import prefuse.render.EdgeRenderer;
import prefuse.render.LabelRenderer;
import prefuse.util.ColorLib;
import prefuse.util.FontLib;
import prefuse.util.GraphicsLib;
import prefuse.util.display.DisplayLib;
import prefuse.util.ui.UILib;
import prefuse.visual.VisualItem;
import prefuse.visual.expression.InGroupPredicate;
import prefuse.visual.sort.TreeDepthItemSorter;
import py.app.Main;
import py.app.alignment.BuildAlign;
import py.app.cluster.BuildCluster;
import py.app.graph.BuildGraph;



public class OneClickControl extends ControlAdapter {
	protected boolean repaint = true;
	private int m_button = LEFT_MOUSE_BUTTON;
	private Main main;
	private String directory,filepath,spiecesname,position,absolutepath;

	private int frame_x,frame_y;
	private boolean isnode=true;

	public void setreadmain(Main main){
		this.main=main;
	}

	public OneClickControl(String directory, String filepath) {
		this.directory = directory; //returns /Daten/tRNA6bakeVerbose
		this.filepath = filepath;   //returns /Daten/tRNA6bakeVerbose/Outtree.txt
	}

/*add a image folder in the data folder,
 * @return the image folder's path*/
	public String bilderFolderPath(){
		File folder = new File(directory);
		if(folder.isDirectory()){
			for(File f : folder.listFiles()){
				if(!f.getName().equalsIgnoreCase("image")){
					File newfolder = new File(directory+"/Image");
					newfolder.mkdir();
					return newfolder.getAbsolutePath();
				}
				else{
					return f.getAbsolutePath();
				}
			}
		}

		return null;
	}

/*the save menu for popup frames
 * @return the JPopupMenu and save the image*/
	public JPopupMenu menu1(){
		JPopupMenu pop = new JPopupMenu();
		JMenuItem item1 = new JMenuItem("Save");
		item1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e){
					try {
						JComponent component = (JComponent)frame.getComponent(0);
						BufferedImage image = new BufferedImage(component.getWidth(), component.getHeight(),
								BufferedImage.TYPE_3BYTE_BGR);
						Graphics2D g = image.createGraphics();
						g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
						g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
						g.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
						component.paint(g);
						String frametitle = frame.getTitle();
						String type = frametitle.substring(frametitle.lastIndexOf("|")+1);
						String newfile = bilderFolderPath()+"/"+spiecesname+"_"+position+"_"+type;

						ImageIO.write(image, "png",new File(newfile+".png"));
						if(new File(newfile+".png").exists()){
							JOptionPane.showMessageDialog(frame, "File path: "+ newfile+".png",
									"The image is saved successfully!",JOptionPane.INFORMATION_MESSAGE);
						}
						}

					 catch (IOException ex) {
						ex.printStackTrace();
					}
			}
		});
		pop.add(item1);
		return pop;
	}


/*add menu items to the popup menu
 * @return a JPopupMenu with all items added*/

	public JPopupMenu menu() {
		JPopupMenu popup = new JPopupMenu();

		if(!isnode){
			ArrayList<String> types = new ArrayList<String>();
			types.add("deletions");
			types.add("duplications");
			types.add("insertions");
			types.add("missing");
			types.add("pseudodeletions");
			types.add("pseudoinsertions");
			types.add("pseudomissing");
			for (int i = 0; i < types.size(); i++) {
				String name = types.get(i);
				String position_line = readLines(name);

				if(position_line!=null){
					JMenu item = new JMenu(name);
					item.add(addItem("Graph",name));
					item.add(addItem("Cluster",name));
					item.add(addItem("Alignment",name));
					popup.add(item);
				}

			}
		}
		else{
			ArrayList<String> types = new ArrayList<String>();
			types.add("deletions");
			types.add("matches");
			types.add("pseudodeletions");
			types.add("pseudomatches");
			for (int i = 0; i < types.size(); i++) {
				String name = types.get(i);
				String position_line = readLines(name);


				if(position_line!=null){
					JMenu item = new JMenu(name);
					item.add(addItem("Graph",name));
					item.add(addItem("Cluster",name));
					item.add(addItem("Alignment",name));
					popup.add(item);
				}

			}
		}


		return popup;
	}


/*find the left and right anchors to the corresponding species in different summary files,
 * @para names are the names in side each summary file
 * @return the needed itemname*/
	public String readLines(String names) {
		String line;
		String itemnames = "";
		File f = new File(directory);
		if (f.isDirectory()) {
			for (File subs : f.listFiles()) {
				if (subs.getName().contains("summary")) {
					String[] parts = subs.getName().split("_");
					if (parts[0].equals(names)) {
						absolutepath = subs.getAbsolutePath();
					}
				}
			}
		}

		try {
			BufferedReader br = new BufferedReader(new FileReader(absolutepath));
			while ((line = br.readLine()) != null) {
				String temp = line.substring(1);

				if (line.startsWith(">") && temp.equals(spiecesname)) {
					itemnames = br.readLine();
					return itemnames;
				}
			}
			br.close();
		} catch (IOException e) {
			Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, e);
		}

		return itemnames;

	}


/*menu that opens cluster, alignments and graph frame,
 * @type the type of the file: cluster, alignment or graph
 * @name the species name
 * @return the JMenu*/
	public JMenu addItem(String type,String name){
		JMenu menu = new JMenu(type);

		String[] positions = readLines(name).split(",");
		for (int i = 0; i < positions.length; i++) {
			JMenuItem item = new JMenuItem(positions[i]);
			item.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ev) {
					position = ev.getActionCommand().toString();
					JPopupMenu me = (JPopupMenu) ((JMenuItem) ev.getSource()).getParent();
					JMenu jm = (JMenu) me.getInvoker();
					String showtype = jm.getText();
					graph_frame(getPath(directory, showtype));
				}
			});
			menu.add(item);
		}

		return menu;
	}

	public void cluster_frame(){

	}

/*find the corresponding file path : cluster, graph, alignment,
 * @folderpath the folder path contains the files,
 * @type the file type, for exampel: cluster, al or gr
 * @return the absolutpath of the file*/
	public String getPath(String folderpath, String type) {
		File f = new File(folderpath);
		if (f.isDirectory()) {
			for (File sub : f.listFiles()) {
				if (sub.isDirectory()) {
					if (sub.getName().toLowerCase().contains(type.toLowerCase())) {
						for(File subsub : sub.listFiles()){
							if(!subsub.isDirectory()&&subsub.getName().contains(position)){
								return subsub.getAbsolutePath();
							}
						}
					}
				}
			}
		}
		 return null;
	}


/*set up different frames with their MouseListeners,
 * each case set up different frame,
 * @path the cooresponding file path*/
	public JPanel panel;
	public JFrame frame;
	public void graph_frame(String path){

		int last = path.lastIndexOf(".");
		String ext = path.substring(last+1);
		switch(ext){
		case "gr":																//frame for graph
			panel = new BuildGraph(path);
			frame = new JFrame(spiecesname+"||"+position+"||"+"GRAPH");
			Display dis = (Display)panel.getComponent(0);
			dis.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					checkPopup(e);
				}

				private void checkPopup(MouseEvent e) {
					if (SwingUtilities.isLeftMouseButton(e)) {
						menu1().show(e.getComponent(), e.getX(), e.getY());
					}
				}

			});
			break;
		case "clus":															//frame for cluster
			panel = new BuildCluster(path);
			panel.setPreferredSize(new Dimension(600,400));
			frame = new JFrame(spiecesname+"||"+position+"||"+"CLUSTER");
			JScrollPane p = (JScrollPane) panel.getComponent(0);
			JViewport comp = (JViewport) p.getComponent(0);
			JTextArea area = (JTextArea)comp.getComponent(0);
			area.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					checkPopup(e);
				}

				private void checkPopup(MouseEvent e) {
					if (SwingUtilities.isLeftMouseButton(e)) {
						menu1().show(e.getComponent(), e.getX(), e.getY());
					}
				}

			});
			break;
		case "aln":																//frame for alignment
			panel = new BuildAlign(path);
			frame = new JFrame(spiecesname+"||"+position+"||"+"ALIGNMENT");
			Component sp = panel.getComponent(0);
			sp.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					checkPopup(e);
				}

				private void checkPopup(MouseEvent e) {
					if (SwingUtilities.isLeftMouseButton(e)) {
						menu1().show(e.getComponent(), e.getX(), e.getY());
					}
				}

			});
			break;
		}

		frame.setLayout(new BorderLayout());
		frame.add(panel,BorderLayout.CENTER);
		frame.setSize(900, 400);
		frame.setLocation(frame_x, frame_y);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setVisible(true);
		frame.pack();
	}


/*
 * set up the popupMenu position,
 * @item the nodes in the tree,
 * @e the mouse event*/
	public void itemClicked(VisualItem item, MouseEvent e) {

		Rectangle2D bounds = item.getBounds();
		Display display = (Display) e.getComponent();
		int x, y;
		double scale = display.getScale();
		x = (int) scale * e.getX() + e.getX();
		y = e.getY() - (int) scale * e.getY();
		int x_1,y_1;
		x_1=(int)scale*e.getX()-e.getX();
		y_1=e.getY()+(int)scale*e.getY();
		String name = item.getSourceTuple().get("Node").toString();


		if (!display.isTranformInProgress() && UILib.isButtonPressed(e, m_button)) {
			if (item.isInGroup("tree.nodes")) {
				if(!(Boolean)item.getSourceTuple().get("isLeaf")){
					isnode=true;
					spiecesname=name.substring(0, name.indexOf("\n"));
				}
				else{
					isnode=false;
					spiecesname=name;
				}

				if (scale == 1) {
					frame_x=(int)bounds.getMaxX();
					frame_y=(int)bounds.getCenterY();
					menu().show(e.getComponent(), (int) bounds.getMaxX(), (int) bounds.getCenterY());
				} else if(scale<1){
					frame_x=(int)bounds.getMaxX();
					frame_y=(int)bounds.getCenterY();
					menu().show(e.getComponent(), x+20, y);
				}
				else{
					frame_x=(int)bounds.getMaxX();
					frame_y=(int)bounds.getCenterY();
					menu().show(e.getComponent(),e.getX()+20,e.getY());
				}
			}
		}
	}

}


