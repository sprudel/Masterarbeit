package py.app.cluster;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import prefuse.util.FontLib;
import py.app.Main.CellRenderer;

public class BuildCluster extends JPanel {
	final static boolean shouldFill = true;
	final static boolean shouldWeightX = true;
	final static boolean RIGHT_TO_LEFT = false;

	/* set up the panel */
	public BuildCluster(String filename) {
		setLayout(new BorderLayout(0, 0));
		add(text(filename), BorderLayout.CENTER);

	}

	/*
	 * read from file to get the informations
	 *
	 * @para filename is the name of the file containing all informations to
	 * visualize
	 */
	public ArrayList<String> readFile(String filename) {
		String line;
		ArrayList<String> lines = new ArrayList<String>();

		try {
			BufferedReader br = new BufferedReader(new FileReader(filename));
			while ((line = br.readLine()) != null) {
				lines.add(line);
			}
			br.close();

		} catch (IOException e) {
			System.out.println(e);
		}
		return lines;
	}

	/*
	 * set the contents in a JPanel with a JTextArea in it
	 *
	 * @para filename is the same filename in method BuildCluster
	 *
	 * @return a JScrollPane containing a JTextArea
	 */
	public JScrollPane text(String filename) {
		JTextArea area = new JTextArea();
		area.setEditable(false);
		area.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 15));
		String format = "%1$-20s %2$-20s %3$-20s %4$-20s %5$-10s %6$-15s %7$-15s %8$-10s %9$-20s %10$-20s \n";
		String format_seq = "%1$-20s %2$S \n";
		String format_str = "%1$-20s %2$-60s \n";
		String header = String.format(format, "Chromosome", "Species_ID", "Start Coordinate", "End Coordinate",
				"Strang", "Block_Left", "Block_Right", "Type", "Pseudogene", "Comments");
		area.append(header);
		for (int i = 0; i < readFile(filename).size(); i++) {
			String info = readFile(filename).get(i);
			String[] details = info.split("\t");
			String informations = String.format(format, details[0], details[1], details[2], details[3], details[4],
					details[5], details[6], details[9], details[10], details[11]);
			area.append(informations);
			String sequence = String.format(format_seq, "Sequence", details[8]);
			area.append(sequence);
			String structure = String.format(format_str, "Structure", details[7]);
			area.append(structure + "\n");
		}

		JScrollPane jsp = new JScrollPane(area, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

		return jsp;
	}
}
