package py.app.graph;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
//import java.awt.geom.Point2D.Double;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import com.sun.prism.paint.Color;

import javafx.embed.swing.JFXPanel;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import prefuse.Constants;
import prefuse.Display;
import prefuse.Visualization;
import prefuse.action.ActionList;
import prefuse.action.RepaintAction;
import prefuse.action.assignment.ColorAction;
import prefuse.action.assignment.DataColorAction;
import prefuse.action.assignment.FontAction;
import prefuse.action.layout.CircleLayout;
import prefuse.action.layout.GridLayout;
import prefuse.action.layout.Layout;
import prefuse.action.layout.RandomLayout;
import prefuse.action.layout.graph.ForceDirectedLayout;
import prefuse.activity.Activity;
import prefuse.controls.DragControl;
import prefuse.controls.PanControl;
import prefuse.controls.ZoomControl;
import prefuse.controls.ZoomToFitControl;
import prefuse.data.Edge;
import prefuse.data.Graph;
import prefuse.data.Node;
import prefuse.data.Schema;
import prefuse.data.Table;
import prefuse.data.Tree;
import prefuse.data.Tuple;
import prefuse.data.column.Column;
import prefuse.data.tuple.TupleSet;
import prefuse.render.DefaultRendererFactory;
import prefuse.render.EdgeRenderer;
import prefuse.render.LabelRenderer;
import prefuse.util.ColorLib;
import prefuse.util.FontLib;
import prefuse.util.GraphicsLib;
import prefuse.util.PrefuseLib;
import prefuse.util.display.DisplayLib;
import prefuse.util.force.ForceSimulator;
import prefuse.visual.DecoratorItem;
import prefuse.visual.EdgeItem;
import prefuse.visual.NodeItem;
import prefuse.visual.VisualItem;
import prefuse.visual.expression.InGroupPredicate;
import prefuse.visual.sort.TreeDepthItemSorter;

public class BuildGraph extends JPanel {
	private Tree graph;

	public static final String EDGE_DECORATORS = "edgeDeco";
	private static final Schema DECORATOR_SCHEMA = PrefuseLib.getVisualItemSchema();
	static {
		DECORATOR_SCHEMA.setDefault(VisualItem.INTERACTIVE, false);
		DECORATOR_SCHEMA.setDefault(VisualItem.TEXTCOLOR, ColorLib.rgb(0, 0, 0));
		DECORATOR_SCHEMA.setDefault(VisualItem.FONT, FontLib.getFont("Tahoma", 14));
	}

	/*
	 * build the panel with two prefuse displays in it
	 *
	 * @parm filename is the file name of the graph it come from
	 * OneclickControl.java in py.app.control
	 */
	public BuildGraph(String filename) {
		graphCreate(filename);
		sidegraph(filename);
		setLayout(new BorderLayout());
		Border line = LineBorder.createBlackLineBorder();
		display.setBorder(line);
		d.setBorder(line);
		add(d, BorderLayout.CENTER);
		add(display, BorderLayout.EAST);
		add(threshold(filename), BorderLayout.NORTH);
		setVisible(true);
	}

	/*
	 * set up the panel to set up the threshold
	 *
	 * @parm filename is the same as in the method BuildGraph()
	 *
	 * @return the panel containing a JTextField and a JButton
	 */
	public JPanel threshold(String filename) {
		JPanel input = new JPanel();
		input.setLayout(new FlowLayout());
		JTextField text = new JTextField(20);
		text.setText("0.0");
		JButton button = new JButton("Filtering");
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				getgr(filename, text.getText());
			}
		});
		input.add(text);
		input.add(button);
		return input;
	}

	/*
	 * check if both labels are higher than the threshold
	 *
	 * @parm labels are the edge labels containing two scores
	 *
	 * @parm input the threshold in the JTextFiled
	 *
	 * @return true if both scores are higher than the threshold, false if at
	 * least one score is lower than the threshold
	 */
	public Boolean overthreshold(String[] labels, double input) {
		double label;
		for (int i = 0; i < labels.length; i++) {
			label = Double.parseDouble(labels[i]);
			if (label < input)
				return false;
			else
				return true;
		}
		return false;
	}

	/*
	 * re-visualize the graph after filtered with the threshold
	 *
	 * @parm filename is the same from method BuildGraph
	 *
	 * @parm text is the threshold user put inside the JTextField
	 */
	public void getgr(String filename, String text) {
		Graph newgraph = graph;
		double input = Double.parseDouble(text);
		Iterator it = vis.items(EDGE_DECORATORS);
		while (it.hasNext()) {
			DecoratorItem item = (DecoratorItem) it.next();
			EdgeItem e = (EdgeItem) item.getDecoratedItem();
			String label = e.get(VisualItem.LABEL).toString();
			int length = label.length();
			label = label.substring(1, length - 1);
			label = label.replaceAll(" ", "");
			String[] split = label.split(",");
			if (!overthreshold(split, input)) {
				item.setVisible(false);
				e.getTargetItem().setVisible(false);
				e.setVisible(false);
				d.repaint();
			} else {
				item.setVisible(true);
				e.getTargetItem().setVisible(true);
				e.setVisible(true);
				d.repaint();
			}
		}

	}

	/*
	 * get node names and scores from file
	 *
	 * @parm filename is the same filename from method BuildGraph
	 *
	 * @return a LinkedHashMap containing node names and scores
	 */
	public LinkedHashMap<List<String>, List<String>> inputstring(String filename) {
		String line;
		LinkedHashMap<List<String>, List<String>> readed = new LinkedHashMap<List<String>, List<String>>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(filename));

			while ((line = br.readLine()) != null) {
				ArrayList<String> nodes = new ArrayList<String>();
				ArrayList<String> weights = new ArrayList<String>();
				String[] subs = line.split(" ");
				nodes.add(subs[0]);
				nodes.add(subs[1]);
				weights.add(subs[2]);
				weights.add(subs[3]);
				readed.put(nodes, weights);
			}
			br.close();

		} catch (IOException e) {
			System.out.println(e);
		}
		return readed;
	}

	private Node start, end;

	/*
	 * get the species_type, also for the species noted with "alt" ect.
	 *
	 * @parm splits is a string array containing whole node name splitted with
	 * "_"
	 *
	 * @return a string with nodes' short name
	 */
	public String nodesnames(String[] splits) {
		StringBuilder sb = new StringBuilder();
		loop: for (int i = 1; i < splits.length; i++) {
			if (!splits[i].matches("[0-9]+")) {
				sb.append(splits[i]);
				break loop;
			}
		}
		return sb.toString();
	}

	/*
	 * visualize the candidates graph
	 *
	 * @parm filename is the same filename in method BuildGraph
	 *
	 * @return a graph
	 */
	public Graph graphen(String filename) {
		graph = new Tree();
		graph.addColumn("Node", String.class);
		graph.addColumn("ID", Integer.class);
		graph.addColumn("Size", Integer.class);
		graph.addColumn("Name", String.class);
		graph.addColumn("isnode", Boolean.class);
		graph.addColumn(VisualItem.LABEL, String.class);

		final Set<Entry<List<String>, List<String>>> mapValues = inputstring(filename).entrySet();
		final int maplength = mapValues.size();
		final Entry<List<String>, List<String>>[] test = new Entry[maplength];
		mapValues.toArray(test);

		ArrayList<String> roots = new ArrayList<String>();
		roots.add(test[0].getKey().get(0));

		for (int i = 0; i < maplength - 1; i++) {
			if (!test[i].getKey().get(0).equals(test[i + 1].getKey().get(0))) {
				roots.add(test[i + 1].getKey().get(0));
			}
		}

		int id = 0;
		for (int i = 0; i < roots.size(); i++) {
			String root = roots.get(i);
			StringBuilder sb = new StringBuilder();
			String[] split = root.split("_");
			sb.append(nodesnames(split));
			sb.append("\n");
			sb.append(split[split.length - 2]);

			Node node = graph.addNode();
			node.set("Node", sb.toString());
			node.set("ID", i);
			node.set("Name", roots.get(i));
			node.set("isnode", true);
			node.set("Size", i * 15 + 15);
			id++;
			for (int j = 0; j < maplength; j++) {
				if (root.equals(test[j].getKey().get(0))) {
					String childname;
					StringBuilder s = new StringBuilder();
					String[] split1 = test[j].getKey().get(1).split("_");
					s.append(nodesnames(split1));
					s.append("\n");
					s.append(split1[split1.length - 2]);

					Node child = graph.addChild(node);
					child.set("Node", s.toString());
					child.set("ID", id);
					child.set("Name", test[j].getKey().get(1));
					child.set("isnode", false);
					child.set("Size", i * 15 + 15);
					List<String> names = new ArrayList<String>();
					names.add(roots.get(i));
					names.add(test[j].getKey().get(1));

					id++;
				}
			}
		}

		for (int i = 0; i < graph.getEdgeCount(); i++) {
			String source, target;
			List<String> names = new ArrayList<String>();
			source = graph.getEdge(i).getSourceNode().get("Name").toString();
			target = graph.getEdge(i).getTargetNode().get("Name").toString();
			names.add(source);
			names.add(target);
			if (inputstring(filename).containsKey(names)) {
				graph.getEdge(i).setString(VisualItem.LABEL, inputstring(filename).get(names).toString());
			}
		}

		return graph;
	}

	private static Visualization vis;
	public static Display d;

	/* set up the visualization of the graph */
	public void setupVis() {
		vis = new Visualization();
		vis.add("graph", graph);
		// DECORATOR_SCHEMA.setDefault(VisualItem.TEXTCOLOR, ColorLib.gray(0));
		vis.addDecorators(EDGE_DECORATORS, "graph.edges", DECORATOR_SCHEMA);
		vis.setInteractive("graph.edges", null, false);
	}

	/*
	 * set up the renderer of the graph
	 */
	public void setupRender() {
		LabelRenderer l = new LabelRenderer("Node");
		l.setRoundedCorner(10, 10);

		EdgeRenderer e = new EdgeRenderer(Constants.EDGE_TYPE_LINE);
		e.setDefaultLineWidth(2.0);

		DefaultRendererFactory drf = new DefaultRendererFactory(l);

		drf.add(new InGroupPredicate("graph.edges"), e);
		drf.add(new InGroupPredicate(EDGE_DECORATORS), new LabelRenderer(VisualItem.LABEL));
		vis.setRendererFactory(drf);
	}

	/* set up the Action of the graph */
	public void setupAction() {
		ColorAction edge = new ColorAction("graph.edges", VisualItem.STROKECOLOR, ColorLib.rgb(0, 0, 0));
		ColorAction text = new ColorAction("graph.nodes", VisualItem.TEXTCOLOR, ColorLib.rgb(0, 0, 0));
		ColorAction fill = new NodeColorAction("graph.nodes");
		ColorAction stroke = new NodeColorAction1("graph.nodes");

		ActionList color = new ActionList();
		color.add(fill);
		color.add(edge);
		color.add(text);
		color.add(stroke);

		graphLayout layout = new graphLayout("graph");

		NodeFontAction font = new NodeFontAction("graph.nodes");

		ActionList labellayout = new ActionList();
		// labellayout.add(new LabelLayout(EDGE_DECORATORS));
		labellayout.add(layout);
		labellayout.add(font);
		labellayout.add(color);
		labellayout.add(new RepaintAction());

		vis.putAction("labellayout", labellayout);

	}

	/* set up the display of the graph */
	public void setupDisplay() {

		d = new Display(vis);
		d.setPreferredSize(new Dimension(800, 800));
		d.zoomAbs(new Point2D.Double(400, 100), 0.5);
		d.repaintImmediate();
		d.setHighQuality(true);
		d.addControlListener(new DragControl());
		d.addControlListener(new PanControl());
		d.addControlListener(new ZoomControl());
		d.addControlListener(new ZoomToFitControl());
		d.setItemSorter(new TreeDepthItemSorter());
	}

	/* create and display the graph */
	public void graphCreate(String filename) {
		graphen(filename);
		setupVis();
		setupRender();
		setupAction();
		setupDisplay();

		vis.run("labellayout");
	}

	/*
	 * @parm gr is the graph created from method sidegraph
	 *
	 * @parm nodeName is the name of the node
	 *
	 * @returns the node id in the graph if the "FullName" are the same
	 */
	public int nodeID(Graph gr, String nodeName) {
		for (int i = 0; i < gr.getNodeCount(); i++) {
			if (gr.getNode(i).get("Name").equals(nodeName)) {
				return i;
			}
		}
		return -1;
	}

	public Display display;
	public Visualization visualization;
	public int nodesinsidegraph;

	/*
	 * creating a co-graph for cluster beside the candidate graphs
	 *
	 * @parm filename is the same filename in method BuildGraph
	 */
	public void sidegraph(String filename) {
		Graph side = new Graph();
		side.addColumn("Node", String.class);
		side.addColumn("ID", Integer.class);
		side.addColumn("Name", String.class);
		side.addColumn("isnode", Boolean.class);

		final Set<Entry<List<String>, List<String>>> mapValues = inputstring(filename).entrySet();
		final int maplength = mapValues.size();
		final Entry<List<String>, List<String>>[] test = new Entry[maplength];
		mapValues.toArray(test);

		LinkedHashMap<String, String> overthresholds = new LinkedHashMap<String, String>();
		ArrayList<List<String>> anchors = new ArrayList<List<String>>();
		ArrayList<List<String>> allnodes = new ArrayList<List<String>>();
		for (int i = 0; i < maplength; i++) {
			List<String> values = test[i].getValue();
			List<String> keys = test[i].getKey();
			String value1 = values.get(0).replaceAll(" ", "");
			String value2 = values.get(1).replaceAll(" ", "");
			String[] value = { value1, value2 };
			allnodes.add(keys);
			if (overthreshold(value, 0.8)) {
				anchors.add(keys);
			}
		}

		ArrayList<String> nodecandidates = new ArrayList<String>();

		if (allnodes.size() > 2) {
			for (int i = 0; i < allnodes.size() - 1; i++) {
				if (i == 0) {
					List<String> candidates = allnodes.get(0);
					nodecandidates.add(candidates.get(0));
				} else {
					List<String> candidate1 = allnodes.get(i);
					List<String> candidate2 = allnodes.get(i + 1);
					if (!candidate1.get(0).equals(candidate2.get(0))) {
						nodecandidates.add(candidate2.get(0));
					}
				}
			}
		} else {
			List<String> candidate = allnodes.get(0);
			nodecandidates.add(candidate.get(0));
			nodecandidates.add(candidate.get(1));
		}

		for (int i = 0; i < nodecandidates.size(); i++) {
			String name = nodecandidates.get(i);
			StringBuilder sb = new StringBuilder();
			String[] split = name.split("_");

			sb.append(nodesnames(split));
			sb.append("\n");
			sb.append(split[split.length - 2]);

			Node node = side.addNode();
			node.set("Node", sb.toString());
			node.set("ID", i);
			node.set("Name", name);
			node.set("isnode", true);
		}
		// the node count in this graph
		nodesinsidegraph = nodecandidates.size();

		for (int i = 0; i < side.getNodeCount(); i++) {
			Node anchor = side.getNode(i);
			String anchorname = anchor.get("Name").toString();
			for (int j = 0; j < anchors.size(); j++) {
				List<String> children = anchors.get(j);
				String childname = children.get(1).toString();
				if (anchorname.equals(children.get(0).toString())) {
					if (nodeID(side, childname) != -1) {
						Node child = side.getNode(nodeID(side, childname));
						if (side.getEdge(anchor, child) == null && side.getEdge(child, anchor) == null) {
							side.addEdge(anchor, child);
						}
					}

				}
			}
		}
		// set up the visualization of this graph
		visualization = new Visualization();
		LabelRenderer l = new LabelRenderer("Node");
		l.setRoundedCorner(10, 10);
		// set up the renderer of this graph
		EdgeRenderer e = new EdgeRenderer(Constants.EDGE_TYPE_LINE);
		e.setDefaultLineWidth(2.0);
		DefaultRendererFactory drf = new DefaultRendererFactory(l);
		drf.add(new InGroupPredicate("side.edges"), e);
		visualization.setRendererFactory(drf);
		visualization.add("side", side);

		// set up the actions of this graph
		ColorAction edge = new ColorAction("side.edges", VisualItem.STROKECOLOR, ColorLib.rgb(0, 0, 0));
		ColorAction text = new ColorAction("side.nodes", VisualItem.TEXTCOLOR, ColorLib.rgb(0, 0, 0));
		NodeColorAction fill = new NodeColorAction("side.nodes");
		ActionList color = new ActionList();
		color.add(fill);
		color.add(edge);
		color.add(text);
		cographLayout lay = new cographLayout("side");
		lay.setLayoutAnchor(new Point2D.Double(100, 100));
		color.add(lay);
		color.add(new RepaintAction());

		visualization.putAction("color", color);
		visualization.run("color");
		// set up the display of this graph
		display = new Display(visualization);
		display.setPreferredSize(new Dimension(600, 600));
		display.panTo(new Point2D.Double(100, 300));
		display.repaintImmediate();
		display.setHighQuality(true);
		display.addControlListener(new DragControl());
		display.addControlListener(new PanControl());
		display.addControlListener(new ZoomControl());
		display.addControlListener(new ZoomToFitControl());
	}

	/* the layout of the candidate graphs */
	public class graphLayout extends Layout {
		private int m_orientation;
		protected NodeItem m_root;
		private double angle, slice, radius, x, y;

		public graphLayout(String group) {
			super(group);
			m_orientation = Constants.ORIENT_LEFT_RIGHT;
		}

		public void run(double frac) {
			Iterator it = vis.items("graph.nodes");

			int count = 0;
			ArrayList<NodeItem> roots = new ArrayList<NodeItem>();
			while (it.hasNext()) {
				NodeItem item = (NodeItem) it.next();
				item.setFixed(true);
				if ((Boolean) item.getSourceTuple().get("isnode")) {
					count++;
					roots.add(item);
				}
			}

			int n = 3;
			int m;
			if (count % n == 0) {
				m = count / n;
			} else {
				m = count / n + count % n;
			}

			Rectangle2D b = getLayoutBounds();
			double bx = b.getMinX(), by = b.getMinY();
			double w = b.getWidth(), h = b.getHeight();

			// layout the nodes in the middle as root in the gridLayout
			for (int i = 0; i < count; i++) {
				NodeItem root = roots.get(i);
				radius = 180;
				x = bx + (w + 530) * ((i % n) / (double) (n - 1));

				if (m > 3 && m < 6) {
					y = by + (h + 9 * radius) * ((i / n) / (double) (m - 1));
				} else if (m >= 6) {
					y = by + (h + 22 * radius) * ((i / n) / (double) (m - 1));
				} else {
					y = by + (h + radius) * ((i / n) / (double) (m - 1));
				}
				setX(root, null, x);
				setY(root, null, y);

				// layout the children nodes of the middle node as a circle
				int childcount = root.getChildCount();
				if (m > 1) {
					slice = (2 * Math.PI) / childcount;
					for (int j = 0; j < childcount; j++) {
						angle = slice * j;
						NodeItem child = (NodeItem) root.getChild(j);
						setX(child, null, x + radius * Math.cos(angle - Math.PI / 2));
						setY(child, null, y + radius * Math.sin(angle - Math.PI / 2));
					}
				} else {
					for (int j = 0; j < childcount; j++) {
						NodeItem child = (NodeItem) root.getChild(j);
						setX(child, root, x);
						setY(child, root, Math.pow(-1, j) * radius);
					}
				}
			}

			Iterator iter = vis.items(EDGE_DECORATORS);
			while (iter.hasNext()) {
				DecoratorItem item = (DecoratorItem) iter.next();
				VisualItem edge = item.getDecoratedItem();
				Rectangle2D bounds = edge.getBounds();
				double x = bounds.getCenterX();
				double y = bounds.getCenterY();
				setX(item, null, x);
				setY(item, null, y - 25);
			}
		}

	}

	/* the layout of the co-graph */
	public class cographLayout extends Layout {
		private double x, y;

		public cographLayout(String group) {
			super(group);
		}

		public void run(double frac) {
			Iterator it = visualization.items("side.nodes");
			ArrayList<NodeItem> single = new ArrayList<NodeItem>();

			ArrayList<NodeItem> nodes = new ArrayList<NodeItem>();
			HashMap<NodeItem, Boolean> candidates = new HashMap<NodeItem, Boolean>();
			while (it.hasNext()) {
				NodeItem item = (NodeItem) it.next();
				if (item.getDegree() != 0) {
					nodes.add(item);
					candidates.put(item, false);
				} else {
					single.add(item);
				}
			}
			int n = 5;
			int m;
			if (single.size() % n == 0) {
				m = single.size() / n;
			} else {
				m = single.size() / n + single.size() % n;
			}

			Rectangle2D b = visualization.getBounds("side");
			double bx = b.getMinX(), by = b.getMinY();
			double w = b.getWidth(), h = b.getHeight();

			for (int i = 0; i < single.size(); i++) {
				NodeItem node = single.get(i);
				x = bx + (w + 150) * ((i % n) / (double) (n - 1));

				if (m > 1) {
					y = by + (h + 100) * ((i / n) / (double) (m - 1));
				} else {
					y = 200;
				}
				setX(node, null, x);
				setY(node, null, y);
			}

			groups(nodes, candidates);
			int groupnum = sets.size();
			double centery = y + 180;
			double centerx = 100;
			double radius = 80;
			double xx = 50;
			double yy = y + 100;

			for (int i = 0; i < groupnum; i++) {
				int nodecounts = sets.get(i).size();
				if (contain()) {
					for (int j = 0; j < nodecounts; j++) {
						NodeItem nn = (NodeItem) sets.get(i).get(j);
						double angle = (2 * Math.PI * j) / nodecounts;
						double x = Math.cos(angle) * radius + centerx;
						double y = Math.sin(angle) * radius + centery;
						setX(nn, null, x);
						setY(nn, null, y);
					}
					centery = centery + 250;

				} else {
					for (int j = 0; j < nodecounts; j++) {
						NodeItem nn = (NodeItem) sets.get(i).get(j);
						setX(nn, null, xx);
						setY(nn, null, yy);
						yy = yy + 100;
					}
				}
			}

		}

		// check if the co-graph contain a circle
		public boolean contain() {
			int groupcount = sets.size();
			for (int i = 0; i < groupcount; i++) {
				int nodecount = sets.get(i).size();
				if (nodecount > 2) {
					return true;
				} else {
					return false;
				}
			}
			return true;
		}

		// each 'sets' contain all nodes that in a connected graph
		private ArrayList<ArrayList<NodeItem>> sets;

		/*
		 * gather all nodes that exist in one connected graph
		 *
		 * @parm nodes is an ArrayList containing all nodes in the graph
		 *
		 * @parm candidates is a HashMap containing nodes and their boolean
		 * values if they have been 'visited'
		 */
		public void groups(ArrayList<NodeItem> nodes, HashMap<NodeItem, Boolean> candidates) {
			sets = new ArrayList<ArrayList<NodeItem>>();
			for (NodeItem v : nodes) {
				if (candidates.get(v) == false) {
					ArrayList<NodeItem> set = new ArrayList<NodeItem>();
					group(set, v, candidates);
					sets.add(set);
				}
			}
		}

		/*
		 * gather all nodes that exist in one connected graph
		 *
		 * @parm set is an ArrayList containing all nodes that connected to
		 * teach other
		 *
		 * @parm v is the NodeItem that need to be check if it has neighbors
		 *
		 * @parm candidates is a HashMap containing nodes and their boolean
		 * values if they have been 'visited'
		 */
		public void group(ArrayList<NodeItem> set, NodeItem v, HashMap<NodeItem, Boolean> candidates) {
			if (candidates.get(v) == false) {
				set.add(v);
				candidates.put(v, true);
				for (NodeItem neighbor : Neighbors(v, candidates)) {
					if (Neighbors(neighbor, candidates) != null) {
						group(set, neighbor, candidates);
					}
				}
			}
		}

		/*
		 * get all neighbors of a node
		 *
		 * @parm item a NodeItem
		 *
		 * @parm candidates is a HashMap containing nodes and their boolean
		 * values if they have been 'visited'
		 */
		public ArrayList<NodeItem> Neighbors(NodeItem item, HashMap<NodeItem, Boolean> candidates) {
			Iterator it = item.neighbors();
			ArrayList<NodeItem> neighbor = new ArrayList<NodeItem>();
			while (it.hasNext()) {
				NodeItem nei = (NodeItem) it.next();
				if (candidates.get(nei) == false) {
					neighbor.add(nei);
				}
			}
			return neighbor;
		}
	}

	/* set up the font of each node */
	public static class NodeFontAction extends FontAction {
		public NodeFontAction(String group) {
			super(group);
		}

		public Font getFont(VisualItem item) {
			if ((Boolean) item.getSourceTuple().get("isnode"))
				return FontLib.getFont("Bold", 25);
			else
				return FontLib.getFont("Bold", 20);
		}
	}

	/*
	 * set up the stroke of each node
	 */
	public static class NodeColorAction1 extends ColorAction {
		public NodeColorAction1(String group) {
			super(group, VisualItem.STROKECOLOR);
		}

		public int getColor(VisualItem item) {
			if ((Boolean) item.getSourceTuple().get("isnode")) {
				return ColorLib.rgb(0, 0, 0);
			}
			return ColorLib.rgb(255, 255, 255);
		}
	}

	// set up the node color for each node
	public static class NodeColorAction extends ColorAction {
		public NodeColorAction(String group) {
			super(group, VisualItem.FILLCOLOR);
		}

		public String nodenames;
		public int nodeids;

		public int equal(VisualItem leaves) {
			String leaf = leaves.getSourceTuple().get("Name").toString();
			Iterator it = vis.items("graph.nodes");
			while (it.hasNext()) {
				VisualItem innode = (VisualItem) it.next();
				if ((Boolean) innode.getSourceTuple().get("isnode")) {
					if (innode.getSourceTuple().get("Name").toString().equals(leaf)) {
						return (int) innode.getSourceTuple().get("ID");
					}
				}
			}
			return 0;
		}

		public int getColor(VisualItem item) {
			int[] colors = new int[] { ColorLib.rgb(166, 206, 227), ColorLib.rgb(31, 120, 180),
					ColorLib.rgb(178, 223, 138), ColorLib.rgb(51, 160, 44), ColorLib.rgb(251, 154, 153),
					ColorLib.rgb(227, 26, 28), ColorLib.rgb(253, 191, 111), ColorLib.rgb(255, 127, 0),
					ColorLib.rgb(202, 178, 214), ColorLib.rgb(106, 61, 154), ColorLib.rgb(255, 255, 153),
					ColorLib.rgb(177, 89, 40) };
			if ((Boolean) item.getSourceTuple().get("isnode")) {
				nodenames = item.getSourceTuple().get("Node").toString();
				nodeids = (int) item.getSourceTuple().get("ID");
				return colors[(int) item.getSourceTuple().get("ID") % 12];
			} else {
				return colors[equal(item) % 12];
			}
		}
	}

}
