package py.app.view;

import javafx.beans.property.SimpleStringProperty;
import javafx.embed.swing.SwingNode;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.StageStyle;
import py.app.Main;
import py.app.model.StringModel;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class LayoutController {
	private Main main;
	public String parselines;
	private SwingNode sn;

	@FXML
	private TextArea textarea;
	@FXML
	private AnchorPane leftpane;
	@FXML
	private SplitPane splitpane;

	public void setreadmain(Main main) {
		this.main = main;

	}

	public LayoutController() {
	}

	/*
	 * clear all running threads
	 */
	@FXML
	private void handleClear() {
		textarea.clear();
		main.resetLines();
		main.folderpath = null;
		leftpane.getChildren().clear();
	}

	/*
	 * shows the tree and Newick format
	 */
	@FXML
	private void handleShow() {
		if (main.getLines() != null) {
			textarea.setText(main.getLines());
			if (main.newickformat(textarea.getText())) {
				main.treeCreate(textarea.getText());

				sn = new SwingNode();
				leftpane.getChildren().add(sn);
				// leftpane.autosize();
				// int width =
				// (int)sn.getParent().getBoundsInParent().getWidth();
				// int height =
				// (int)sn.getParent().getBoundsInParent().getHeight();

				int width = (int) leftpane.getWidth();
				int height = (int) leftpane.getHeight();

				JInternalFrame frame = main.jframe(main.getLines());
				frame.getComponent(0).setPreferredSize(new Dimension(width, height - 50));
				frame.pack();
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						sn.setContent(frame);
						// sn.prefWidth(height);
					}
				});

				sn.autosize();
			} else {
				main.errorAlert1();
			}
		} else {
			main.errorAlert2();
		}

	}

	/*
	 * find the absolute path of the image folder, if the folder exist,
	 * otherwise the image folder will be made
	 */
	private String bilderFolderPath() {
		File folder = new File(main.folderpath);
		for (File f : folder.listFiles()) {
			if (!f.getName().equalsIgnoreCase("image")) {
				File newfolder = new File(main.folderpath + "/Image");
				newfolder.mkdir();
				return newfolder.getAbsolutePath();
			} else {
				return f.getAbsolutePath();
			}
		}
		return null;
	}

	/*
	 * save the tree image with its table
	 */
	public String filename;

	@FXML
	private void handleSave() {
		String file = main.filepath;
		if (file != null && !leftpane.getChildren().isEmpty()) {
			// if(){
			String[] splitname = file.split("/");
			// String filename;
			for (int i = 0; i < splitname.length; i++) {
				if (splitname[i].contains("bakeVerbose")) {
					int len = "bakeVerbose".length();
					int l = splitname[i].length();
					filename = splitname[i].substring(0, (l - len));
				}
			}

			try {
				JInternalFrame internalframe = (JInternalFrame) sn.getContent();
				JComponent component = (JComponent) internalframe.getComponent(0);
				BufferedImage image = new BufferedImage(component.getWidth(), component.getHeight(),
						BufferedImage.TYPE_3BYTE_BGR);
				// paints into image's Graphics
				// Graphics g = image.getGraphics();
				Graphics2D g = image.createGraphics();
				g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
				g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
				g.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
				component.paint(g);
				String newfile = bilderFolderPath() + "/" + filename + ".png";
				ImageIO.write(image, "png", new File(newfile));
				if (new File(newfile).exists()) {
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("The image is saved successfully!");
					alert.setHeaderText(null);
					alert.setContentText("File path: " + newfile);
					alert.setResizable(true);
					alert.showAndWait();
				} else {
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setHeaderText(null);
					alert.setTitle("INFORMATION");
					alert.setContentText("The image is NOT saved!");
					alert.showAndWait();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		} else {
			main.errorAlert2();
		}

	}
}
