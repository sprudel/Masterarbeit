package py.app.view;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import py.app.Main;
import py.app.model.StringModel;

public class RootLayoutController {
	private Main main;

	public void setreadmain(Main main) {
		this.main = main;
	}

	public String lines;

	/*
	 * open the filechooser to open the tree file
	 */
	@FXML
	private void handleOpen() {

		FileChooser filechooser = new FileChooser();
		String directory = System.getProperty("user.home");
		File defaultDirectory = new File(directory);
		filechooser.setInitialDirectory(defaultDirectory);

		FileChooser.ExtensionFilter extfilter1 = new FileChooser.ExtensionFilter("TEXT Files(*.txt)", "*.txt");
		FileChooser.ExtensionFilter extfilter3 = new FileChooser.ExtensionFilter("ALL Files", "*.*");
		filechooser.getExtensionFilters().addAll(extfilter3, extfilter1);
		filechooser.setTitle("Open File");

		File file = filechooser.showOpenDialog(main.getPrimaryStage());

		if (file != null) {
			lines = main.parse(file);
		} else {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error Dialog");
			alert.setHeaderText("Error!");
			alert.setContentText("The file is empty!");
			alert.showAndWait();
		}

	}

	/*
	 * exit the application
	 */
	@FXML
	private void handleExit() {
		ButtonType cancle = new ButtonType("CANCEL", ButtonBar.ButtonData.CANCEL_CLOSE);
		ButtonType ok = new ButtonType("YES", ButtonBar.ButtonData.OK_DONE);
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Confirmation Dialog");
		alert.setHeaderText("Existing Program!");
		alert.setContentText("Do you want to exit this program?");
		alert.getButtonTypes().clear();
		alert.getButtonTypes().add(cancle);
		alert.getButtonTypes().add(ok);
		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ok) {
			System.exit(0);
		}
	}

	/*
	 * open the summary file
	 */
	@FXML
	private void handleAbout() {
		// String filename = main.filepath;
		String foldername = main.folderpath;
		if (foldername != null) {
			File folder = new File(foldername);
			for (File f : folder.listFiles()) {
				if (f.getName().equals("Summary.txt")) {
					JFrame frame = new JFrame("Summary.txt");
					frame.setLayout(new BorderLayout());
					JTextArea area = new JTextArea(40, 60);
					area.setLineWrap(true);
					area.setEditable(false);
					try {
						BufferedReader br = new BufferedReader(new FileReader(f));
						area.read(br, null);
						br.close();
					} catch (IOException e) {
						System.out.println(e);
					}
					JScrollPane sp = new JScrollPane(area);
					frame.add(sp, BorderLayout.CENTER);
					frame.setSize(400, 600);
					frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
					frame.pack();
					frame.setVisible(true);
				}
			}
		} else {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error!");
			alert.setContentText("No file is opened!");
			alert.showAndWait();
		}

	}

}
