package py.app.alignment;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.SwingConstants;

public class BuildAlign extends JPanel {
	final static boolean shouldFill = true;
	final static boolean shouldWeightX = true;
	final static boolean RIGHT_TO_LEFT = false;

	private int marks, counts;

	// set up the panel
	public BuildAlign(String filename) {
		setLayout(new BorderLayout());
		add(align(filename), BorderLayout.CENTER);
	}

	/*
	 * get the informations from file
	 *
	 * @para filename is the name of the file containing informations to
	 * visualize
	 *
	 * @return a HashMap contains 'anchor' species and corresponding
	 * informations
	 */
	public HashMap<ArrayList<String>, ArrayList<String>> readFile(String filename) {
		String line;
		LinkedHashMap<ArrayList<String>, ArrayList<String>> map = new LinkedHashMap<ArrayList<String>, ArrayList<String>>();
		ArrayList<String> lines = new ArrayList<String>();

		try {
			BufferedReader br = new BufferedReader(new FileReader(filename));
			counts = 0;
			while ((line = br.readLine()) != null) {
				lines.add(line);
				if (line.contains("@")) {
					counts++;
				}
			}
			br.close();

		} catch (IOException e) {
			System.out.println(e);
		}

		for (int i = 0; i < lines.size(); i++) {
			if (lines.get(i).contains("@")) {
				marks = i;
				String[] split = lines.get(i).split("\t");
				ArrayList<String> anchors = new ArrayList<String>();
				for (int j = 0; j < split.length; j++) {
					anchors.add(split[j]);
				}
				ArrayList<String> compair = new ArrayList<String>();
				for (int j = 1; j <= (counts - 1) * 3; j++) {
					String temp = lines.get(i + j);
					if (temp.contains(">")) {
						String[] temps = temp.split("\t");
						compair.add(temps[0].substring(1));
						compair.add(temps[1]);
					} else {
						compair.add(temp);
					}
				}
				map.put(anchors, compair);
			}
		}
		return map;
	}

	/*
	 * set up the matrix containing alignments
	 *
	 * @para filename is the same in method BuildAlign
	 */
	public JPanel align(String filename) {
		JPanel panel = new JPanel(new GridBagLayout());
		panel.setBackground(Color.white);
		panel.setForeground(Color.black);
		GridBagConstraints c = new GridBagConstraints();
		if (RIGHT_TO_LEFT) {
			panel.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		}
		JLabel label;

		label = new JLabel("Species", SwingConstants.CENTER);
		label.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 15));
		label.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		label.setPreferredSize(new Dimension(100, 100));
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 0;
		panel.add(label, c);

		final Set<Entry<ArrayList<String>, ArrayList<String>>> values = readFile(filename).entrySet();
		final int maplength = values.size();
		final Entry<ArrayList<String>, ArrayList<String>>[] test = new Entry[maplength];
		values.toArray(test);

		ArrayList<ArrayList<String>> details = new ArrayList<ArrayList<String>>();

		int col = 1, row = 1;
		for (int i = 0; i < maplength; i++) {
			String species = test[i].getKey().get(0).substring(1);
			String seq = test[i].getKey().get(1);

			String text = "<html>" + species + "<br/>" + "(" + seq + ")" + "</html>";
			// set first row
			label = new JLabel(text, SwingConstants.CENTER);
			label.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 15));
			label.setBorder(BorderFactory.createLineBorder(Color.BLACK));
			label.setPreferredSize(new Dimension(100, 100));
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = col;
			c.gridy = 0;
			panel.add(label, c);

			// set first column
			label = new JLabel(text, SwingConstants.CENTER);
			label.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 15));
			label.setBorder(BorderFactory.createLineBorder(Color.BLACK));
			label.setPreferredSize(new Dimension(100, 100));
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 0;
			c.gridy = row;
			panel.add(label, c);
			row++;
			col++;
			details.add(test[i].getValue());

		}

		int last_x = 0, last_y = 0;
		for (int i = 0; i < details.size(); i++) {
			ArrayList<String> detail = details.get(i);
			int rows = 1;
			for (int j = 0; j < detail.size(); j = j + 4) {
				String s1 = detail.get(j); // species to compair
				String s2 = detail.get(j + 1); // score
				String s3 = detail.get(j + 2); // anchor seqeunce
				String s4 = detail.get(j + 3); // compare sequence
				String string = "<html>" + s3 + "<br/>" + s4 + "<br/>" + s2 + "</html>";

				if (rows == i + 1) {
					label = new JLabel(" ", SwingConstants.CENTER);
					label.setBorder(BorderFactory.createLineBorder(Color.BLACK));
					label.setPreferredSize(new Dimension(100, 100));
					c.fill = GridBagConstraints.HORIZONTAL;
					c.gridx = i + 1;
					c.gridy = rows;
					panel.add(label, c);
					rows++;
				}
				if (rows > i + 1) {
					label = new JLabel(" ", SwingConstants.CENTER);
					label.setBorder(BorderFactory.createLineBorder(Color.BLACK));
					label.setPreferredSize(new Dimension(100, 100));
					c.fill = GridBagConstraints.HORIZONTAL;
					c.gridx = i + 1;
					c.gridy = rows;
					panel.add(label, c);
					rows++;
				}
				if (rows < i + 1) {
					label = new JLabel(string, SwingConstants.CENTER);
					label.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 15));
					label.setBorder(BorderFactory.createLineBorder(Color.BLACK));
					label.setPreferredSize(new Dimension(100, 100));
					c.fill = GridBagConstraints.HORIZONTAL;
					c.gridx = i + 1;
					c.gridy = rows;
					panel.add(label, c);
					rows++;
				}
			}
			last_x = rows;
			last_y = i + 1;
		}

		label = new JLabel(" ", SwingConstants.CENTER);
		label.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		label.setPreferredSize(new Dimension(100, 100));
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = last_x;
		c.gridy = last_y;
		panel.add(label, c);

		return panel;
	}

}
