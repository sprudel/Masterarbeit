package py.app;

import javafx.application.Application;
import javafx.stage.Stage;
import py.app.control.OneClickControl;
import py.app.model.StringModel;
import py.app.view.LayoutController;
import py.app.view.RootLayoutController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

import prefuse.render.DefaultRendererFactory;
import prefuse.render.EdgeRenderer;
import prefuse.render.LabelRenderer;
import prefuse.util.ColorLib;
import prefuse.util.FontLib;
import prefuse.util.GraphicsLib;
import prefuse.util.PrefuseLib;
import prefuse.util.display.DisplayLib;
import prefuse.util.force.ForceItem;
import prefuse.util.ui.UILib;
import prefuse.visual.DecoratorItem;
import prefuse.visual.EdgeItem;
import prefuse.visual.NodeItem;
import prefuse.visual.VisualItem;
import prefuse.visual.expression.InGroupPredicate;
import prefuse.visual.sort.TreeDepthItemSorter;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
//import java.awt.geom.Rectangle2D.Double;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.AbstractAction;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;


import prefuse.Constants;
import prefuse.Display;
import prefuse.Visualization;
import prefuse.action.Action;
import prefuse.action.ActionList;
import prefuse.action.RepaintAction;
import prefuse.action.animate.ColorAnimator;
import prefuse.action.animate.LocationAnimator;
import prefuse.action.animate.QualityControlAnimator;
import prefuse.action.animate.VisibilityAnimator;
import prefuse.action.assignment.ColorAction;
import prefuse.action.assignment.DataColorAction;
import prefuse.action.assignment.FontAction;
import prefuse.action.filter.FisheyeTreeFilter;
import prefuse.action.layout.Layout;
import prefuse.action.layout.RandomLayout;
import prefuse.action.layout.graph.ForceDirectedLayout;
import prefuse.activity.Activity;
import prefuse.activity.SlowInSlowOutPacer;
import prefuse.controls.Control;
import prefuse.controls.ControlAdapter;
import prefuse.controls.DragControl;
import prefuse.controls.HoverActionControl;
import prefuse.controls.PanControl;
import prefuse.controls.WheelZoomControl;
import prefuse.controls.ZoomControl;
import prefuse.controls.ZoomToFitControl;
import prefuse.data.Edge;
import prefuse.data.Graph;
import prefuse.data.Node;
import prefuse.data.Schema;
import prefuse.data.Table;
import prefuse.data.Tree;
import prefuse.data.Tuple;
import prefuse.data.column.Column;

public class Main extends Application {
	private Stage primaryStage;
	private BorderPane rootLayout;
	private AnchorPane layout;
	private Scene scene;

/*Set the primary open window
 * from method start to method showLayout
 * */
	@Override
	public void start(Stage primaryStage) throws IOException {
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("Tree");


		initRootLayout();
		showLayout();

	}


	public void initRootLayout() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("view/RootLayout.fxml"));

			rootLayout = (BorderPane) loader.load();

			scene = new Scene(rootLayout);
			primaryStage.setScene(scene);

			RootLayoutController controller = loader.getController();
			controller.setreadmain(this);
			primaryStage.show();
			primaryStage.setMaximized(true);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public BorderPane getroot() {
		return rootLayout;
	}

	public Scene getanchorscene(){
		return primaryStage.getScene();
	}

	public void showLayout() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("view/Layout.fxml"));
			layout = (AnchorPane) loader.load();
			rootLayout.setCenter(layout);

			LayoutController controller = loader.getController();
			controller.setreadmain(this);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
/*get the primary stage
 * */
	public Stage getPrimaryStage() {
		return primaryStage;
	}




	public String parselines;
	public String stringtoread;
	public String filepath,folderpath;

	/*read string from opened file with ()
 * @para file is the file that opened
 * */
	public String read(File file) {
		StringBuilder lines = new StringBuilder();
		String line;
		filepath=file.getAbsolutePath();
		folderpath = file.getParent();

		try {
			BufferedReader br = new BufferedReader(new FileReader(file));

			while ((line = br.readLine()) != null) {
				if (line.startsWith("(")) {
					lines.append(line);
				}
			}
			br.close();
			stringtoread = lines.toString();


		} catch (IOException e) {
			Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, e);
		}
		return stringtoread;
	}

//get the file path
	public String setFilepath(){
		return filepath;
	}
//get the folder path that the file is in
	public String setFolderpath(){
		return folderpath;
	}

//clean up the string from file, for building the tree
	public String parse(File file) {

		parselines = read(file).replaceAll("\\[(.*?)\\]", "");

		return parselines;
	}

	private py.app.model.StringModel stringmodel = new StringModel();

//pass the string to stringmodel to use in py.read.view.LayoutController.java
	public String getLines() {
		stringmodel.setText(parselines);
		return stringmodel.getText();
	}
//clear the text
	public void resetLines(){
		if(parselines!=null){
			parselines=null;
			stringmodel.setText(null);
		}
	}


//check if the input string is a newick format
	public boolean newickformat(String lines) {

		// check if the input string is a newick format
		Stack<Character> stack = new Stack<Character>();

		if (lines.contains("(") || lines.contains(")")) {
			for (int i = 0; i < lines.length(); i++) {
				if (lines.charAt(i) == '(') {
					stack.push(lines.charAt(i));
				}

				else if (lines.charAt(i) == ')') {
					if (stack.empty()) {
						return false;
					} else if (stack.peek() == '(') {
						stack.pop();
					} else
						return false;
				}
			}
		} else {
			return false;
		}
		return stack.empty();
	}

//error alert will be triggered when the input is not a newick format
	public void errorAlert1() {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Error Dialog");
		alert.setHeaderText("Error!");
		alert.setContentText("It's not a valid NewickFormat!");
		alert.showAndWait();
	}

//error alert will be triggered when there is no input
	public void errorAlert2(){
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Error Dialog");
		alert.setHeaderText("Error!");
		alert.setContentText("There is no input file!");
		alert.showAndWait();
	}


//get only the first(outer) pair of parenthesis position
	private int getClosingParenthesis(String lines) {
		int depth = 0;

		for (int i = 0; i < lines.length(); i++) {
			if (lines.charAt(i) == '(') {
				depth++;
			}
			if (lines.charAt(i) == ')' && (--depth == 0)) {
				return i;
			}
		}
		return -1;
	}


	private static Tree tree;
	private static Visualization vis;
	public static Display d;


//set up the tree structure
	public Tree tree(){
		tree=new Tree();
		tree.addColumn("Node", String.class);
		tree.addColumn("ID", Integer.class);
		tree.addColumn("isLeaf", Boolean.class);
		return tree;
	}

//insert each node
	public Tree nodes(String lines,int id){
		ArrayList<String> subs = new ArrayList<String>();

		String name="";
		Node node;

		if (lines.contains(";"))
			lines = lines.substring(0, lines.indexOf(";")).trim(); // remove ;

		while (lines.charAt(0) == '(' && lines.indexOf(",", getClosingParenthesis(lines)) == -1) {
			name = lines.substring(getClosingParenthesis(lines) + 1);
			if (id == 0) {
				node = tree().addRoot();
				node.set("Node", name);
				node.set("ID", id);
				node.set("isLeaf", false);

			} else {
				node = tree.addNode();
				node.set("Node", name);
				node.set("ID", id);
				node.set("isLeaf", false);

			}
			lines = lines.substring(1, getClosingParenthesis(lines)).trim();
			id++;

		}

		int nParens = 0;
		int start = 0;
		for (int i = 0; i < lines.length(); i++) {
			switch (lines.charAt(i)) {
			case ',':
				if (nParens == 0) {
					subs.add(lines.substring(start, i));

					start = i + 1;
				}
				break;
			case '(':
				nParens++;
				break;
			case '[':
				nParens++;
				break;
			case ']':
				nParens--;
				break;
			case ')':
				nParens--;
				if (nParens < 0)
					errorAlert1();
				break;
			}
		}
		if (nParens > 0)
			errorAlert1();
		subs.add(lines.substring(start));

		for(int i =0;i<subs.size();i++){
			if(subs.get(i).contains(",")){

				nodes(subs.get(i),id);

			}
			else{
				node = tree.addNode();
				node.set("Node", subs.get(i));
				node.set("ID", id);
				node.set("isLeaf", true);

			}
		}

		return tree;
	}


	//based on the child-parent relationship from each node, edges are added
	public void phyloTree(String lines, int id) {
		Table tab = nodes(lines, id).getNodeTable();
		Column col = tab.getColumn("ID");

		for (int i = 0; i < tab.getRowCount() - 1; i++) {
			if ((Integer) tab.get(i, "ID") < (Integer) tab.get(i + 1, "ID")) {
				tree.addChildEdge(i, i + 1);
			} else if ((Integer) tab.get(i, "ID") > (Integer) tab.get(i + 1, "ID")) {

				inloop: for (int j = i; j > 0; j--) {
					if ((Integer) tab.get(i + 1, "ID") == col.get(j)) {
						tree.addChildEdge(tree.getParent(tree.getNode(j)), tree.getNode(i + 1));
						break inloop;
					}
				}

			} else {
				tree.addChildEdge(tree.getParent(tree.getNode(i)), tree.getNode(i + 1));
			}

		}
	}



//get the detail informations from input string
	public Map<String,List<String>> det(){
		Map<String,List<String>> detail = new LinkedHashMap<String,List<String>>();
		Table tab = nodes(stringtoread, 0).getNodeTable();

		for(int i =0;i<tab.getRowCount();i++){
			String temp;
			String name;
			temp = tab.get(i,"Node").toString();
			int a = temp.indexOf("[");
			int b = temp.indexOf("]");
			name=temp.substring(0, a);
			temp=temp.substring(a+1, b).trim();
			String[] numbers = temp.split("\\|");
			List<String> num = new ArrayList<String>();
			for(int j = 0; j < numbers.length;j++){
				num.add(numbers[j].toString());
			}
			detail.put(name, num);
		}
		return detail;
	}


//fill the table with the details extract from the input string
	public DefaultTableModel tmodel(String lines) {
		Table tab = nodes(lines, 0).getNodeTable();
		Column col1 = tab.getColumn("Node");
		Column col2 = tab.getColumn("isLeaf");
		Object[] identifiers = new Object[] { "Total", "Insertions", "Deletions", "Duplications",
				"Singletons", "Excluded","Missing" };


		DefaultTableModel model = new DefaultTableModel();

		model.addColumn("Leaf Name", identifiers);
		for (int i = 0; i < tab.getRowCount(); i++) {
			if ((Boolean) col2.get(i)) {
				model.addColumn(col1.get(i));
			}
		}

		//Map<String,List<String>> detail = det();
		//DefaultTableModel model = tmodel(lines);
		Iterator it = vis.items("tree.nodes");


		while(it.hasNext()){
			NodeItem item = (NodeItem)it.next();
			List<String> value = new ArrayList<String>();
			String name = item.getSourceTuple().get("Node").toString();
			Boolean leaf = (Boolean)item.getSourceTuple().get("isLeaf");



			StringBuilder sb = new StringBuilder();

			value = det().get(name);

				sb.append(name);
				for(int i = 0; i< value.size();i++){
					String temp = value.get(i).toString();
					String type = temp.substring(0, 1);
					String number = temp.substring(1);
					int column = model.findColumn(name);

					switch(type){
					case "t":
						if(leaf){
							model.setValueAt(number, 0, column);
						}

						break;
					case "i":
						if(leaf){
						model.setValueAt(number, 1, column);
						}
						else{
							item.set("Node", sb.append("\n"+"+"+number+" ").toString());
							d.repaintImmediate();;
						}
						break;
					case "l":
						if(leaf){
							model.setValueAt(number, 2, column);
						}
						else{
							item.set("Node", sb.append("-"+number+" ").toString());
							d.repaintImmediate();
						}

						break;
					case "d":
						if(leaf){
							model.setValueAt(number, 3, column);
						}

						break;
					case "s":
						if(leaf){
							model.setValueAt(number, 4, column);
						}
						/*else{
							item.set("Node", sb.append("s"+number).toString());
							d.repaintImmediate();
						}*/

						break;
					case "n":
						if(leaf){
							model.setValueAt(number, 5, column);
						}

						break;
					case "m":
						if(leaf){
							model.setValueAt(number, 6, column);
						}
						break;

					}

				}

			//System.out.println();
		}
		return model;
	}


//create the JInternalFrame containing phylogenetic tree and table of genetic information
	public JInternalFrame jf_all;
	public JInternalFrame jframe (String lines){

		JTable table = new JTable(tmodel(lines));
		table.getTableHeader().setFont(FontLib.getFont("Serif",Font.PLAIN,12));
		table.setBackground(Color.WHITE);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

		CellRenderer cellrenderer = new CellRenderer();
		cellrenderer.setHorizontalAlignment(JLabel.CENTER);
		cellrenderer.setFont(FontLib.getFont("Serif", Font.BOLD,20));

		DefaultTableCellRenderer firstcolumn = new DefaultTableCellRenderer();
		firstcolumn.setFont(FontLib.getFont("Serif", Font.BOLD,20));
		table.getColumnModel().getColumn(0).setCellRenderer(firstcolumn);

		for(int i = 1; i< table.getColumnCount();i++){
				table.getColumnModel().getColumn(i).setCellRenderer(cellrenderer);

		}


		TableColumn column = table.getColumn("Leaf Name");
		column.setPreferredWidth("Duplications".length()*10);

		JScrollPane sp = new JScrollPane(table);
		sp.setPreferredSize(new Dimension(800, 150));
		sp.setBackground(Color.WHITE);
		sp.setForeground(Color.BLACK);


		//String title = setFilepath().substring(setFilepath().indexOf("_")+1, setFilepath().indexOf("."));
		int last = setFolderpath().lastIndexOf("/");
		String title = setFolderpath().substring(last+1);
		jf_all=new JInternalFrame(title);
		JPanel panel = new JPanel();
		//panel.setPreferredSize(new Dimension(800,700));
		panel.setLayout(new BorderLayout());
		panel.add(sp,BorderLayout.SOUTH);
		panel.add(d,BorderLayout.CENTER);
		jf_all.getContentPane().add(panel);
		//jf_all.setPreferredSize(new Dimension(1000,700));
		jf_all.setVisible(true);
		//jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf_all.setResizable(true);
		jf_all.pack();
		jf_all.setClosable(true);
		jf_all.setEnabled(true);

		return jf_all;
	}


//set up the visualization
	public static void setupVis() {
		vis = new Visualization();
		vis.add("tree", tree);
		vis.setInteractive("tree.edges", null, false);
	}


//set up the renderer
	public void setupRender() {
		LabelRenderer l = new LabelRenderer("Node");
		l.setRoundedCorner(10,10);
		l.setHorizontalAlignment(Constants.CENTER);

		EdgeRenderer e = new EdgeRenderer(Constants.EDGE_TYPE_LINE);

		DefaultRendererFactory drf = new DefaultRendererFactory(l);

		drf.add(new InGroupPredicate("tree.edges"), e);
		vis.setRendererFactory(drf);


	}

//set up actions for the tree output
	public void setupAction() {

		ColorAction edge = new ColorAction("tree.edges", VisualItem.STROKECOLOR, ColorLib.rgb(0,0,0));
		ColorAction fill = new ColorAction("tree.nodes",VisualItem.FILLCOLOR,ColorLib.rgb(255, 255, 255));
		ColorAction text = new ColorAction("tree.nodes", VisualItem.TEXTCOLOR, ColorLib.rgb(50, 50, 50));
		ColorAction circle = new ColorAction("tree.nodes", VisualItem.STROKECOLOR,ColorLib.rgb(0, 0, 0));
		ActionList color = new ActionList();
		color.add(fill);
		color.add(edge);
		color.add(text);
		color.add(circle);

		phyloLayout tlayout= new phyloLayout("tree");
		tlayout.setLayoutAnchor(new Point2D.Double(400,50));

		FontAction font = new NodeFontAction("tree.nodes");
		vis.putAction("font", font);

		ActionList layout = new ActionList();
		layout.add(tlayout);
		layout.add(font);
		layout.add(color);
		layout.add(new RepaintAction());
		vis.putAction("layout", layout);

	/*	ActionList filter = new ActionList();
		filter.add(font);
		filter.add(tlayout);
        filter.add(text);
        filter.add(fill);
        filter.add(edge);
        vis.putAction("filter", filter);*/

  /*      ActionList animate = new ActionList(1000);
        animate.setPacingFunction(new SlowInSlowOutPacer());
        animate.add(new QualityControlAnimator());
        animate.add(new VisibilityAnimator("tree"));
        animate.add(new LocationAnimator("tree.nodes"));
        animate.add(new ColorAnimator("tree.nodes"));
        animate.add(new RepaintAction());
        vis.putAction("animate", animate);
        vis.alwaysRunAfter("filter", "animate");*/

  /*      ActionList animatePaint = new ActionList(400);
        animatePaint.add(new ColorAnimator("tree.nodes"));
        animatePaint.add(new RepaintAction());
        vis.putAction("animatePaint", animatePaint);*/

        ActionList fullPaint = new ActionList();
        fullPaint.add(fill);
        vis.putAction("fullPaint", fullPaint);


	}



//set up the display with visualization in it
	public void setupDisplay() {

		d = new Display(vis);
		d.setPreferredSize(new Dimension(1000,1000));
		d.zoomAbs(new Point2D.Double(800,50), 0.65);
		d.setHighQuality(true);
		d.addControlListener(new WheelZoomControl());
		d.addControlListener(new DragControl());
		d.addControlListener(new PanControl());
		d.addControlListener(new ZoomControl());
		d.addControlListener(new ZoomToFitControl());
		d.setItemSorter(new TreeDepthItemSorter());
		d.addControlListener(new OneClickControl(folderpath,filepath));
	}


//create tree
	public void treeCreate(String lines) {
		phyloTree(lines,0);
		setupVis();
		setupRender();
		setupAction();
		setupDisplay();
		vis.run("layout");
	}


	public static void main(String[] args) {
		launch(args);
	}


//customize the text font for each node in the tree
	public static class NodeFontAction extends FontAction {
		public NodeFontAction(String group) {
			super(group);
		}

		public Font getFont(VisualItem item) {

			if ((Boolean) item.getSourceTuple().get("isLeaf")) {
				return FontLib.getFont("Tahoma", 20);
			} else {
				return FontLib.getFont("Tahoma", 11);
			}

		}
	}

//customize the color, position and font for each cell in the table
	public class CellRenderer extends DefaultTableCellRenderer {

		public CellRenderer() {
		      super();
		   }

		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
				int row, int column) {
			Component cell = super.getTableCellRendererComponent(table, value, false, false, row, column);
			Color[] color = {new Color(76, 0, 153),Color.red,Color.blue,new Color(102,0,51),new Color(0,102,0),Color.black,new Color(204,102,0)};
			if(color[row]!=null){
				cell.setForeground(color[row]);
			}
			else{
				cell.setForeground(getForeground());
			}
			return cell;
		}
	}


/*	customize the fill color for each node in the tree
 * public static class NodeColorAction extends ColorAction {
		public NodeColorAction(String group) {
			super(group, VisualItem.FILLCOLOR);
		}

		public int getColor(VisualItem item) {
			if ((Boolean)item.getSourceTuple().get("isLeaf"))
				return ColorLib.rgb(229, 255, 204);
			else
				return ColorLib.rgb(255,255,255);
		}
	}*/

//customize the layout for the tree
	public class phyloLayout extends Layout {
		private int m_orientation;
		protected NodeItem m_root;
		public phyloLayout(String group) {
			super(group);
			m_orientation = Constants.ORIENT_LEFT_RIGHT;
		}


		public int getOrientation(){
			return m_orientation;
		}



		private double Y;
		private double X;

//collect y-coordinate from each node
		public ArrayList<Double> sety(){
			ArrayList<Double> ys = new ArrayList<Double>();
			ys.add(Y);
			return ys;
		}

//collect x-coordinate from each node
		public ArrayList<Double> setx(){
			ArrayList<Double> xs = new ArrayList<Double>();
			xs.add(X);
			return xs;
		}


//get the minimum x-coordinate from all x-coordinates
		public double getx(){
			if(setx()==null){
				errorAlert1();
				return 0.0;
			}
			else{
				double min = setx().get(0);
				for(int i = 0;i<setx().size();i++){
					double temp = setx().get(i);
					if(temp<min)
						min=temp;
				}
				return min;
			}
		}

//get the maximum y-coordinate from all y-coordinates
		public double gety(){
			if(sety()==null){
				errorAlert1();
				return 0.0;
			}
			else{
				double max = sety().get(0);
				for(int i = 0;i<sety().size();i++){
					double temp = sety().get(i);
					if(temp>max)
						max=temp;
				}
				return max;
			}
		}


//collect the leaves of the tree
		public ArrayList<NodeItem> leaves(){
			ArrayList<NodeItem> leaf = new ArrayList<NodeItem>();
			Iterator it = vis.items("tree.nodes");
			while(it.hasNext()){
				NodeItem item = (NodeItem)it.next();
				if((Boolean)item.getSourceTuple().get("isLeaf")){
					leaf.add(item);
				}
			}
			return leaf;
		}


//collect the in-nodes of the tree
		public ArrayList<NodeItem> innode(){
			ArrayList<NodeItem> node=new ArrayList<NodeItem>();
			Iterator it = vis.items("tree.nodes");
			while(it.hasNext()){
				NodeItem item = (NodeItem)it.next();
				if(!(Boolean)item.getSourceTuple().get("isLeaf")){
					node.add(item);
				}
			}
			return node;
		}


//collect all edges in the tree
		public ArrayList<VisualItem> edges(){
			ArrayList<VisualItem> edge = new ArrayList<VisualItem>();
			Iterator it = vis.items("tree.edges");
			while(it.hasNext()){
				VisualItem item = (VisualItem)it.next();
					edge.add(item);
			}
			return edge;
		}

//set all leaves with new positions
		public void updateleaves(){
			double spring=0;
			double allwidth=0;
			double viswidth =vis.getBounds("tree").getWidth();

			for(int i = 0; i<innode().size();i++){
				int childcount = innode().get(i).getChildCount();
				NodeItem parent = (NodeItem)innode().get(i);
				NodeItem first_child = (NodeItem)parent.getFirstChild();
				if((Boolean)first_child.getSourceTuple().get("isLeaf")
						&& first_child.equals(leaves().get(0))){
					double width = first_child.getBounds().getWidth();
					if(width>20){
						setX(first_child,null,-first_child.getBounds().getWidth()*8);
					}
					else{
						setX(first_child,null,-first_child.getBounds().getWidth()*10);
					}

				}
				for(int j = 1; j<leaves().size();j++){
					NodeItem first = (NodeItem)leaves().get(j-1);
					NodeItem second = (NodeItem)leaves().get(j);
					Rectangle2D bounds = first.getBounds();
					double width = bounds.getWidth();

					if(width>25){
						setX(second,first,first.getX()+width*3);
					}
					else if(width<25&&width>18){
						setX(second,first,first.getX()+width*6.5);
					}
					else{
						setX(second,first,first.getX()+width*10);
					}
				}
			}

			for(int i = 0; i<leaves().size();i++){
				NodeItem leaf = leaves().get(i);
				setY(leaf, null, gety()+100);
			}

			d.repaintImmediate();
		}


//set all positions of nodes besides leaves in the tree
		public void run(double frac){
			Iterator it  = vis.items("tree.nodes");
			Point2D anchor = getLayoutAnchor();
			while(it.hasNext()){
				NodeItem node = (NodeItem)it.next();
				int child_count = node.getChildCount();
				int spring = 0;

				if((Integer) node.getSourceTuple().get("ID")==0){
					setX(node,null,anchor.getX());
					setY(node,null,anchor.getY());

					for(int i = 0; i<child_count;i++){
						NodeItem child = (NodeItem) node.getChild(i);
						NodeItem pre = (NodeItem)child.getPreviousSibling();
						if (!(Boolean) child.getSourceTuple().get("isLeaf")) {
							setY(child, node, node.getY() + 80);
							Y = child.getY();
							double width=child.getBounds().getWidth();
							if (pre == null) {
								setX(child, node, node.getX()-80  + spring);
							}
							else {
								if ((Boolean) pre.getSourceTuple().get("isLeaf")){
									setX(child, node, node.getX() +20 + spring);
									}
								else {
									if (width > 48)
										setX(child, pre, pre.getX() + pre.getBounds().getWidth() * 2.5);
									else
										setX(child, pre, pre.getX() + pre.getBounds().getWidth() * 20);
								}
							}

							X = child.getX();

							double x1 = child.getBounds().getMinX();
							double center = child.getBounds().getCenterX();
							if (child.getBounds().getWidth() > 100)
								spring += x1 + width - center + width * 1.5;
							else
								spring += x1 + width - center + width * 15;
						}
					}
				}
				else{
					for(int i = 0; i<child_count;i++){
						NodeItem child = (NodeItem) node.getChild(i);
						NodeItem pre = (NodeItem)child.getPreviousSibling();

						if(!(Boolean)child.getSourceTuple().get("isLeaf")){
							setY(child,node,node.getY()+80);
							Y=child.getY();
							double width=child.getBounds().getWidth();
							if (pre == null) {
								NodeItem uncle = (NodeItem) child.getParent().getPreviousSibling();
								if (uncle != null&&!(Boolean) uncle.getSourceTuple().get("isLeaf")) {
										setX(child, node, uncle.getBounds().getMaxX());
								} else
									setX(child, node, node.getX() - 60 + spring);
							}
							else{
								if((Boolean) pre.getSourceTuple().get("isLeaf")){
									NodeItem next = (NodeItem)child.getNextSibling();
									if(next!=null){
										if((Boolean)next.getSourceTuple().get("isLeaf")){
											setX(child, node, node.getX()  + spring);
										}
										else{
											setX(child, node, node.getX()+20  + spring);
										}
									}
									else{
										NodeItem tante = (NodeItem)child.getParent().getNextSibling();

										if(tante!=null){
											if((Boolean)tante.getSourceTuple().get("isLeaf")){
												setX(child, node, node.getX()+20+spring);
											}
											else{
												setX(child, node, node.getBounds().getMaxX());
											}
										}
										else{
											setX(child,node,node.getX()+20+spring);
											//System.out.println(child);
										}
									}

								}
								else {
									if (width > 45)
										setX(child, pre, pre.getX() + pre.getBounds().getWidth() * 2);
									else
										setX(child, pre, pre.getX() + pre.getBounds().getWidth() * 20);
								}
							}

							X=child.getX();
							double x1 = child.getBounds().getMinX();
							double center = child.getBounds().getCenterX();
							if (child.getBounds().getWidth() > 100)
								spring += x1 + width - center + width * 1.5;
							else
								spring += x1 + width - center + width * 15;
						}

					}
				}
			}

			updateleaves();
		}


	}
}
