package py.app.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import py.app.view.LayoutController;
import py.app.view.RootLayoutController;

public class StringModel {
	private final StringProperty lines;

	public StringModel(){
		this(null);
	}
/*set the string property*/
	public StringModel(String line) {
		this.lines = new SimpleStringProperty(line);
	}
/*get the string's text*/
	public String getText(){
		return lines.get();
	}
/*
 * set the string's text*/
	public void setText(String line){

		this.lines.set(line);
	}



	public StringProperty textProperty(){
		return lines;
	}


}
