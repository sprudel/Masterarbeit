# SMOREvis - SMORE visualization tool

This is the readme file of **SMOREvis**, a visualization tool for the **SMORE** pipeline.

## Start SMOREvis

Open terminal and write the command line: **java -jar whole/file/path/SMOREvis.jar**, to start the SMOREvis.

## Usages of SMOREvis

### General functions

- **_Click_ right mouse button** on the screen to adjust the tree's/graph's position to fit the screen.
- **_Hold_ right mouse button** on the screen to zoom-in or zoom-out from the tree.
- **Hold left mouse button** on nodes to drag them to any other position.

### Phylogenetic tree

After starting **SMOREvis**, clicking on the menu bar **_File_->_Open_** to open the tree file. After that clicking on the **_Show Tree_** button to show the phylogenetic tree and its information table on the screen. 

Clicking on the **_Save_** button, the user can save the phylogenetic tree and its table as an image. Clicking on the **_Clear_** button, the user can clear all the threads and open a new tree file to see the visualization.

### Graphs, Cluster and Alignment

Clicking on any node to choose either "Graph", "Cluster" or "Alignment" in the menu, the corresponding frame will be popped up. In each frame, the user can click on the **_left mouse button_** to save the visualization as an image.

There are some special functions in the frames of graphs and cluster. 

- Graphs

In the graphs frame, the user can set the threshold and click on the "Filter" button on the top of the frame to show only the edges with both labels are higher than the threshold.

- Cluster

In the cluster frame, besides saving it as an image, the user can also use the mouse to choose the specific lines and copy the lines if needed.

## Outputs

After the images are successfully saved, there will be an information showing on the screen. Normally the images are saved in a folder named **_Image_** in the data folder.
